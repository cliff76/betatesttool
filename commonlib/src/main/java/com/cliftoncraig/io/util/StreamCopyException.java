package com.cliftoncraig.io.util;

import java.io.IOException;

/**
 * Created by clifton
 * Copyright 6/9/15.
 */
public class StreamCopyException extends Exception {
    public StreamCopyException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
