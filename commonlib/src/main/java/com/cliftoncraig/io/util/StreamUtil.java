package com.cliftoncraig.io.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamUtil {
    public static final int KILOBYTE = 1024;
    public static final int MEGABYTE = 1024 * KILOBYTE;
    public interface StreamCopyListener {

        void onProgress(int totalProcessed, long size);
    }
    StreamCopyListener listener;

    public StreamUtil(StreamCopyListener listener) {
        this.listener = listener;
    }

    public void copyStreams(long size, InputStream inputStream, OutputStream outputStream) throws StreamCopyException {
        int totalProcessed = 0;
        byte[] buffer = new byte[MEGABYTE];
        try {
            for(int byteCount = inputStream.read(buffer); byteCount!=-1; byteCount = inputStream.read(buffer)) {
                outputStream.write(buffer, 0, byteCount);
                totalProcessed+=byteCount;
                if (listener!=null) {
                    listener.onProgress(totalProcessed, size);
                }
            }
        }
        catch (IOException e) {
            throw new StreamCopyException("Could not copy response stream ", e);
        }
    }
}
