package com.cliftoncraig.web.updates;

import com.cliftoncraig.io.util.StreamCopyException;
import com.cliftoncraig.io.util.StreamUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by clifton
 * Copyright 6/25/15.
 */
public class UpdateServlet extends HttpServlet implements StreamUtil.StreamCopyListener {
    final static String UPDATE_TEMPLATE = "{\n" +
            "  \"package\": \"com.cliftoncraig.beta\",\n" +
            "  \"version\": \"0.0.162764\",\n" +
            "  \"apk\": \"chat.apk\"\n" +
            "}";
    public static final int RESPONSE_OK = 200;
    StreamUtil streamUtil = new StreamUtil(this);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String requestType = req.getPathInfo();
        if(requestType.endsWith("meta.json")) {
            respondWithMetaData(resp);
        } else if(requestType.endsWith(".apk")) {
            respondWithApk(requestType, resp);
        }
    }

    private void respondWithApk(String requestType, HttpServletResponse resp) throws ServletException {
        final ServletOutputStream outputStream;
        try { outputStream = resp.getOutputStream(); }
        catch (IOException e) { throw new ServletException("Could not get output stream for response!", e); }
        final URL url = getClass().getResource("/chat.apk");
        final File file;
        try { file = new File(url.toURI()); }
        catch (URISyntaxException e) { throw new RuntimeException("Bad URI syntax" + url, e); }
        final InputStream inputStream;
        try { inputStream = new FileInputStream(file); }
        catch (FileNotFoundException e) { throw new RuntimeException("could not find apk file" + file, e); }
        try { streamUtil.copyStreams(file.length(), inputStream, outputStream); }
        catch (StreamCopyException e) { throw new ServletException("Error copying apk stream to response.", e); }
    }

    private void respondWithMetaData(HttpServletResponse response) throws ServletException {
        final PrintWriter writer;
        try { writer = response.getWriter(); }
        catch (IOException e) { throw new ServletException("Cannot get writer for response!", e); }
        response.setStatus(RESPONSE_OK);
        writer.append(UPDATE_TEMPLATE);
        writer.flush();
    }

    @Override
    public void onProgress(int totalProcessed, long size) {

    }
}
