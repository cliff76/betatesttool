package com.cliftoncraig.betatest.updatetool;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

public class UpdateActivityBroadcastReceiver extends BroadcastReceiver {
    interface UpdateListener {
        void onDownloadComplete(File downloadFile);

        void onUpdateCheck();

        void onUpToDate();

        void onProgress(float percentage);

        void onDownloadFailed(Uri uri, String reason);
    }

    private final String TAG = getClass().getName();

    private UpdateListener updateListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Uri uri = null;
        if (intent.hasExtra(Constants.EXTENDED_DATA_STATUS)) {
            uri = (Uri) intent.getExtras().get(Constants.EXTENDED_DATA_STATUS);
        }
        final String action = intent.getAction();
        if (action==null) {
            Log.d(TAG, "Received broadcast with no action!");
        } else if (uri==null) {
            Log.d(TAG, "Received " + action + " broadcast with no extended data extra.");
            handleSimpleAction(intent);
        } else if (action.equals(Constants.BROADCAST_ACTION_BEGIN_UPDATE_CHECK)) {
            Log.d(TAG, "Received BROADCAST_ACTION_BEGIN_UPDATE_CHECK broadcast with " + uri.getClass() + ": " + uri);
            onUpdateCheck(uri);
        } else if (action.equals(Constants.BROADCAST_ACTION_DOWNLOAD_COMPLETED)) {
            onDownloadComplete(asFile(uri));
        } else if (action.equals(Constants.BROADCAST_ACTION_DOWNLOAD_FAILED)) {
            onDownloadFailed(uri, intent.getStringExtra(Constants.ERROR_REASON));
        } else {
            Log.d(TAG, "Received UKNOWN broadcast!");
        }
    }

    private void onDownloadFailed(Uri uri, String reason) {
        updateListener.onDownloadFailed(uri, reason);
    }

    private void handleSimpleAction(Intent intent) {
        final String action = intent.getAction();
        if (action.equals(Constants.BROADCAST_ACTION_UP_TO_DATE)) {
            onUpToDate();
        } else if (action.equals(Constants.BROADCAST_ACTION_PROGRESS)) {
            final float percentage = intent.getFloatExtra(Constants.BROADCAST_EXTRAS_PROGRESS, -1.0f);
            Log.d(TAG, "Received BROADCAST_ACTION_PROGRESS broadcast " + percentage + "%");
            onProgress(percentage);
        }
    }

    private void onProgress(float percentage) {
        updateListener.onProgress(percentage);
    }

    private void onUpToDate() {
        updateListener.onUpToDate();
    }

    private void onUpdateCheck(Uri uri) {
        if(updateListener != null) {
            updateListener.onUpdateCheck();
        }
    }

    private File asFile(Uri uri) {
        final File installFile;
        try { installFile = new File(new URI(String.valueOf(uri))); }
        catch (URISyntaxException e) { throw new RuntimeException("Invalid install uri " + uri); }
        return installFile;
    }

    private void onDownloadComplete(File downloadFile) {
        if (updateListener != null) {
            updateListener.onDownloadComplete(downloadFile);
        }
    }

    public void setUpdateListener(UpdateActivity updateListener) {
        this.updateListener = updateListener;
    }
}
