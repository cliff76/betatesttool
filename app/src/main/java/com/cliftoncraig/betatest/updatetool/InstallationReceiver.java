package com.cliftoncraig.betatest.updatetool;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class InstallationReceiver extends BroadcastReceiver {
    private final String TAG = getClass().getName();

    public InstallationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Installation action " + intent.getAction());
        if(intent.getData().toString().equals("package:com.sgiggle.production")) {
            final String androidId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Log.d(TAG, "Sending Device id " + androidId);
            new AsyncTask<String, Void, Void>() {
                @Override
                protected Void doInBackground(String... params) {
                    sendDeviceId(params[0]);
                    return null;
                }
            }.execute(androidId);
        }
    }

    private void sendDeviceId(String androidId) {
        final String spec = "http://10.0.0.6:8080/apk/ack.php?androidID=" + androidId;
        final URL url;
        try { url = new URL(spec); }
        catch (MalformedURLException e) { throw new RuntimeException("Invalid URL " + spec); }
        final long responseCode;
        try { responseCode = ((HttpURLConnection) url.openConnection()).getResponseCode(); }
        catch (IOException e) { Log.e(TAG, "Could not connect to device id server."); return; }
        Log.d(TAG, "DeviceID update request sent! " + responseCode);
    }
}
