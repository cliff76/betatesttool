package com.cliftoncraig.betatest.updatetool;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.cliftoncraig.io.util.StreamCopyException;
import com.cliftoncraig.net.URLUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by clifton
 * Copyright 6/8/15.
 */
public class URLUpdateManager extends UpdateManager {

    static UpdateManager instance;

    protected URLUpdateManager(Context context) {
        super(context);
    }

    public static synchronized UpdateManager getInstance(Context context) {
        if (instance==null) {
            instance = new URLUpdateManager(context);
        }
        return instance;
    }

    @Override
    protected long getDownloadSize(String apk) {
        final HttpURLConnection connection = new URLUtil().getHeadHttpURLConnection(getApkUrl(apk));
        final String contentLength = connection.getHeaderField("Content-Length");
        return Long.parseLong(contentLength);
    }

    private final String TAG = getClass().getName();

    protected File downloadTo(String apk, File downloadFile) throws StreamCopyException {
        final HttpURLConnection connection = new URLUtil().getHttpURLConnection(getApkUrl(apk));
        dumpHeaders(connection);
        final String contentLength = connection.getHeaderField("Content-Length");
        streamUtil.copyStreams(Long.parseLong(contentLength), getInputStreamFromUrl(connection), getOutputStreamFromfile(downloadFile));
        return downloadFile;
    }

    @Override
    protected File downloadAdditionalTo(long fileSize, String apk, File downloadFile) throws StreamCopyException {
        final HttpURLConnection connection = new URLUtil().getHttpURLConnection(getApkUrl(apk));
        connection.setRequestProperty("Range", "bytes=" + downloadFile.length() + "-");
        dumpHeaders(connection);
        final String contentLength = connection.getHeaderField("Content-Length");
        streamUtil.copyStreams(Long.parseLong(contentLength), getInputStreamFromUrl(connection), getOutputStreamFromfile(downloadFile, true));
        return downloadFile;
    }

    private InputStream getInputStreamFromUrl(HttpURLConnection connection) {
        final InputStream inputStream;
        try { inputStream = connection.getInputStream(); }
        catch (IOException e) { throw new RuntimeException("Could not open response stream from URL connection " + connection, e); }
        return inputStream;
    }

    private void dumpHeaders(HttpURLConnection urlConnection) {
        final Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
        for(Map.Entry<String,List<String>> eachEntry : headerFields.entrySet()) {
            Log.d(TAG, eachEntry.getKey() + " = " + TextUtils.join(", ", eachEntry.getValue()));
        }
    }

    private OutputStream getOutputStreamFromfile(File resultFile) {
        return getOutputStreamFromfile(resultFile, false);
    }

    private OutputStream getOutputStreamFromfile(File resultFile, boolean append) {
        final OutputStream fileOutputStream;
        try { fileOutputStream = new FileOutputStream(resultFile, append); }
        catch (FileNotFoundException e) {
            throw new RuntimeException("Could not download to output file " + resultFile.getAbsolutePath(), e);
        }
        return fileOutputStream;
    }

    protected URL getApkUrl(String apk) {
        final Uri apkUri = Uri.withAppendedPath(Uri.parse(Constants.UPDATE_URL_BASE), apk);
        Log.d(TAG, "Downloading apk " + apkUri);
        try { return new URL(apkUri.toString()); }
        catch (MalformedURLException e) {
            throw new RuntimeException("Invalid apk URL "  +apk);
        }
    }
}
