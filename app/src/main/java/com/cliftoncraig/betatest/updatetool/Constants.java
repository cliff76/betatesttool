package com.cliftoncraig.betatest.updatetool;

/**
 * Created by clifton
 * Copyright 6/7/15.
 */
class Constants {
    public static final String SERVICE_ACTION_SET_ALARM = "com.cliftoncraig.betatest.updatetool.SERVICE_ACTION_SET_ALARM";
    public static final String SERVICE_ACTION_UPDATE_CHECK = "com.cliftoncraig.betatest.updatetool.SERVICE_ACTION_UPDATE_CHECK";
    public static final String BROADCAST_ACTION_BEGIN_UPDATE_CHECK = "com.cliftoncraig.betatest.updatetool.BROADCAST_ACTION_BEGIN_UPDATE_CHECK";
    public static final String BROADCAST_ACTION_DOWNLOAD_COMPLETED = "com.cliftoncraig.betatest.updatetool.BROADCAST_ACTION_DOWNLOAD_COMPLETED";
    public static final String BROADCAST_ACTION_DOWNLOAD_FAILED = "com.cliftoncraig.betatest.updatetool.BROADCAST_ACTION_DOWNLOAD_FAILED";
    public static final String BROADCAST_ACTION_UP_TO_DATE = "com.cliftoncraig.betatest.updatetool.BROADCAST_ACTION_UP_TO_DATE";
    public static final String BROADCAST_ACTION_PROGRESS = "com.cliftoncraig.betatest.updatetool.BROADCAST_ACTION_PROGRESS";
    public static final String EXTENDED_DATA_STATUS = "EXTENDED_DATA_STATUS";
    public static final String UPDATE_URL_BASE = "http://10.0.0.6:8080/apk/";
    public static final String UPDATE_URL = UPDATE_URL_BASE + "meta.json";
    public static final String BROADCAST_EXTRAS_PROGRESS = "com.cliftoncraig.betatest.updatetool.BROADCAST_EXTRAS_PROGRESS";
    public static final String BROADCAST_ACTION_BACKGROUND_STATUS = "com.cliftoncraig.betatest.updatetool.BROADCAST_ACTION_BACKGROUND_STATUS";
    public static final String ERROR_REASON = "ERROR_REASON";
}
