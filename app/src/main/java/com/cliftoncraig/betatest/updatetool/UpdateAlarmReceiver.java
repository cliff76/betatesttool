package com.cliftoncraig.betatest.updatetool;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class UpdateAlarmReceiver extends BroadcastReceiver {
    private final String TAG = getClass().getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        final Uri uri = intent.getData();
        Log.d(TAG, "Scheduled update checking using Uri " + uri);
        broadcastUpdateCheck(context, uri);
        final Intent updateServiceIntent = new Intent(context, UpdateService.class);
        updateServiceIntent.setData(uri);
        updateServiceIntent.setAction(Constants.SERVICE_ACTION_UPDATE_CHECK);
        context.startService(updateServiceIntent);
    }

    private void broadcastUpdateCheck(Context context, Uri uri) {
        Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION_BEGIN_UPDATE_CHECK)
                        .putExtra(Constants.EXTENDED_DATA_STATUS, uri);
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
    }
}
