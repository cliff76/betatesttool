package com.cliftoncraig.betatest.updatetool;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.cliftoncraig.ui.PhotoCycler;
import com.cliftoncraig.ui.SunriseView;
import com.filippudak.ProgressPieView.ProgressPieView;

import java.io.File;


public class UpdateActivity extends Activity implements UpdateActivityBroadcastReceiver.UpdateListener {

    public static final String KEY_STATUS = "status";
    private UpdateActivityBroadcastReceiver updateActivityBroadcastReceiver;
    private TextView textStatus;
    private ProgressPieView progressPieView;

    private SunriseView sunriseView;
    Handler handler = new Handler();
    PhotoCycler photoCycler;
    private final String TAG = getClass().getName();
    private boolean failedDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        updateActivityBroadcastReceiver = new UpdateActivityBroadcastReceiver();
        updateActivityBroadcastReceiver.setUpdateListener(this);
        this.textStatus = (TextView) findViewById(R.id.text_status);
        progressPieView = (ProgressPieView) findViewById(R.id.progressPieView);
        progressPieView.setVisibility(View.INVISIBLE);
        sunriseView = (SunriseView) findViewById(R.id.circleView);
        sunriseView.hide();
        photoCycler = new PhotoCycler(this, (ImageView)findViewById(R.id.firstImageView), (ImageView)findViewById(R.id.secondImageView));

        textStatus.setText(getString(R.string.update_idle));
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            handler.postDelayed(createStartingAnimations(),1000);
        }
    }

    private Runnable createStartingAnimations() {
        return new Runnable() {
            @Override
            public void run() {
                sunriseView.rise();
                photoCycler.cycleImages();
            }
        };
    }

    private void listenForAction(String action) {
        // The filter's action is SERVICE_ACTION_UPDATE_CHECK
        IntentFilter statusIntentFilter = new IntentFilter(
                action);

        // Registers the DownloadStateReceiver and its intent filters
        LocalBroadcastManager.getInstance(this).registerReceiver(
                updateActivityBroadcastReceiver,
                statusIntentFilter);
    }

    private void broadcastBackgroundStatus(boolean inBackground) {
        Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION_BACKGROUND_STATUS)
                        .putExtra(Constants.EXTENDED_DATA_STATUS, inBackground);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    private void popInProgressView(OvershootInterpolator interpolator) {
        progressPieView.setScaleX(0);
        progressPieView.setScaleY(0);
        progressPieView.setVisibility(View.VISIBLE);
        progressPieView.animate().setInterpolator(interpolator).scaleX(1.0f);
        progressPieView.animate().setInterpolator(interpolator).scaleY(1.0f);
    }

    public void onGoClicked(View button) {
        final Intent serviceIntent = new Intent(this, UpdateService.class);
        serviceIntent.setAction(Constants.SERVICE_ACTION_SET_ALARM);
        serviceIntent.setData(Uri.parse(Constants.UPDATE_URL));
        final OvershootInterpolator interpolator = new OvershootInterpolator();
        popInProgressView(interpolator);
        startService(serviceIntent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_STATUS, String.valueOf(textStatus.getText()));
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        textStatus.setText(savedInstanceState.getString(KEY_STATUS));
    }

    @Override
    protected void onStart() {
        super.onStart();
        sunriseView.hide();
        progressPieView.setVisibility(View.INVISIBLE);
        final String[] allActions = {
                Constants.BROADCAST_ACTION_BEGIN_UPDATE_CHECK,
                Constants.BROADCAST_ACTION_UP_TO_DATE,
                Constants.BROADCAST_ACTION_PROGRESS,
                Constants.BROADCAST_ACTION_DOWNLOAD_FAILED,
                Constants.BROADCAST_ACTION_DOWNLOAD_COMPLETED
        };
        for(String eachAction : allActions) {
            listenForAction(eachAction);
        }
        broadcastBackgroundStatus(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        photoCycler.stop();
        if (updateActivityBroadcastReceiver !=null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(updateActivityBroadcastReceiver);
        }
        broadcastBackgroundStatus(true);
    }

    @Override
    public void onDownloadComplete(File downloadFile) {
        Intent promptInstall = new Intent(Intent.ACTION_VIEW);
        promptInstall.setDataAndType(Uri.fromFile(downloadFile), "application/vnd.android.package-archive");
        startActivity(promptInstall);
    }

    @Override
    public void onUpdateCheck() {
        this.failedDownload = false;
        textStatus.setText(getString(R.string.status_checking));
    }

    @Override
    public void onUpToDate() {
        textStatus.setText(getString(R.string.status_up_to_date));
        progressPieView.setProgress(100);
    }

    @Override
    public void onProgress(float percentage) {
        int actualPercent = (int) (percentage * 100);
        textStatus.setText(getString(R.string.status_progress, actualPercent));
        progressPieView.setProgress(actualPercent);
    }

    @Override
    public void onDownloadFailed(Uri uri, String reason) {
        this.failedDownload = true;
        textStatus.setText(reason);
    }
}
