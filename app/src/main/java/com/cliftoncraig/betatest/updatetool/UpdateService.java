package com.cliftoncraig.betatest.updatetool;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Created by clifton
 * Copyright 6/7/15.
 */
public class UpdateService extends IntentService {

    private final String TAG = getClass().getName();
    private final UpdateManager updateManager;
    private static final int SECONDS = 1000;
    private static final int MINUTES = 60 * SECONDS;
    private static final int IMMEDIATE_INTERVAL = 0;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public UpdateService(String name) {
        super(name);
        updateManager = URLUpdateManager.getInstance(this);
    }

    /**
     * Creates the Update IntentService using the default name.
     */
    public UpdateService() {
        this("UpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final Uri uri = intent.getData();
        final String action = intent.getAction();
        switch (action) {
            case Constants.SERVICE_ACTION_UPDATE_CHECK:
                Log.d(TAG, "Beginning Update with uri [" + uri + "]");
                updateManager.updateIfNecessary(uri);
                break;
            case Constants.SERVICE_ACTION_SET_ALARM:
                Log.d(TAG, "Registering alarm with uri [" + uri + "]");
                setAlarm(uri);
                break;
            default:
                Log.d(TAG, "Unknown action [" + action + "]");
                break;
        }
    }

    private void setAlarm(Uri uri) {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, UpdateAlarmReceiver.class);
        intent.setData(uri);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        alarmMgr.cancel(alarmIntent);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                IMMEDIATE_INTERVAL,
                5 * MINUTES, alarmIntent);
    }
}
