package com.cliftoncraig.betatest.updatetool;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.cliftoncraig.io.util.StreamCopyException;
import com.cliftoncraig.io.util.StreamUtil;
import com.cliftoncraig.net.JSONUtil;
import com.cliftoncraig.net.UriToJSONException;

import org.json.JSONObject;

import java.io.File;
import java.util.Map;

/**
 * Created by clifton
 * Copyright 6/7/15.
 */
abstract class UpdateManager implements StreamUtil.StreamCopyListener {
    private static final String VERSION = "version";
    private static final String PACKAGE = "package";
    private static final String APK = "apk";
    public static final String APP_UPDATES_DIR_NAME = "appUpdates";
    private final String TAG = getClass().getName();
    private final Context context;
    private final JSONUtil jsonUtil;
    protected final StreamUtil streamUtil;
    private boolean appIsInBackground;
    BroadcastReceiver broadcastReceiver;

    protected UpdateManager(Context context) {
        this.context = context;
        this.jsonUtil = new JSONUtil();
        this.streamUtil = new StreamUtil(this);
        IntentFilter statusIntentFilter = new IntentFilter(
                Constants.BROADCAST_ACTION_BACKGROUND_STATUS);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final boolean isInBackground = intent.getBooleanExtra(Constants.EXTENDED_DATA_STATUS, false);
                setAppIsInBackground(isInBackground);
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver( broadcastReceiver,statusIntentFilter);
    }

    public void updateIfNecessary(Uri uri) {
        JSONObject jsonObject;
        try { jsonObject = jsonUtil.getJsonObject(uri); }
        catch (UriToJSONException e) {
            sendDownloadFailedBroadcast(uri.toString(), "Update server returned a bad response.");
            return;
        }

        Map<String,String> map = jsonUtil.toMapWithKeys(jsonObject, APK, PACKAGE, VERSION);
        if (!isUpToDate(map)) {
            downloadApk(map.get(APK));
        } else {
            sendUpToDateBroadcast();
        }
    }

    private boolean isUpToDate(Map<String, String> map) {
        boolean upToDate;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(map.get(PACKAGE), 0);
            upToDate = packageInfo.versionName.equals(map.get(VERSION));
        } catch (PackageManager.NameNotFoundException e) {
            upToDate = false;
        }
        return upToDate;
    }

    private void downloadApk(String apk) {
        File downloadFile;
        try {
            downloadFile = doDownload(apk);
        } catch (StreamCopyException e) {
            e.printStackTrace();
            sendDownloadFailedBroadcast(apk, "Update download was interrupted.");
            return;
        }
        if (appIsInBackground) {
            sendDownloadCompleteNotification(downloadFile);
        } else {
            sendDownloadCompleteBroadcast(downloadFile);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void sendDownloadCompleteNotification(File downloadFile) {
        final Intent installIntent = new Intent(Intent.ACTION_VIEW);
        installIntent.setDataAndType(Uri.fromFile(downloadFile), "application/vnd.android.package-archive");
        final PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, installIntent, 0);
        final Notification notification = new Notification.Builder(context)
                .setSmallIcon(R.drawable.beta_logo)
                .setTicker("App update ready to install!")
                .setWhen(System.currentTimeMillis())
                .setContentText("Install the latest App update.")
                .setContentIntent(pendingIntent)
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1,notification);
    }

    private File doDownload(String apk) throws StreamCopyException {
        final File externalStorageDirectory = Environment.getExternalStorageDirectory();
        File downloadFile = getDownloadFileName(apk, externalStorageDirectory);
        if (!downloadFile.exists()) {
            makeDownloadDirectory(downloadFile.getParentFile());
            downloadFile = downloadTo(apk, downloadFile);
            Log.d(TAG, "Downloaded to " + downloadFile.getAbsolutePath());
        } else {
            Log.d(TAG, "Download file already exists " + downloadFile.getAbsolutePath());
            long fileSize = getDownloadSize(apk);
            final long length = downloadFile.length();
            onProgress((int)length, fileSize);
            final long remaining = fileSize - length;
            if (remaining > 0) {
                downloadFile = downloadAdditionalTo(fileSize, apk, downloadFile);
            }
        }
        return downloadFile;
    }

    protected abstract File downloadAdditionalTo(long fileSize, String apk, File downloadFile) throws StreamCopyException;

    protected abstract long getDownloadSize(String url);

    private void makeDownloadDirectory(File downloadDirectory) {
        if(!downloadDirectory.exists() && ! downloadDirectory.mkdirs()) {
            throw new RuntimeException("Could not create output directory " + downloadDirectory.getAbsolutePath());
        }
    }

    private void sendDownloadCompleteBroadcast(File downloadFile) {
        Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION_DOWNLOAD_COMPLETED)
                        .putExtra(Constants.EXTENDED_DATA_STATUS, Uri.fromFile(downloadFile));
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
    }

    private void sendDownloadFailedBroadcast(String uri, String reason) {
        Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION_DOWNLOAD_FAILED)
                        .putExtra(Constants.EXTENDED_DATA_STATUS, Uri.parse(uri))
                        .putExtra(Constants.ERROR_REASON, reason);
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
    }

    private void sendUpToDateBroadcast() {
        Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION_UP_TO_DATE);
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
    }

    protected abstract File downloadTo(String url, File directory) throws StreamCopyException;

    protected File getDownloadFileName(String url, File outputDirectory) {
        final File downloadDirectory = new File(outputDirectory, APP_UPDATES_DIR_NAME);
        final String file = Uri.parse(url).getLastPathSegment();
        return new File(downloadDirectory, file);
    }

    public void onProgress(int processed, long totalSize) {
        float percentage = (float)processed/totalSize;
        sendProgress(percentage);
    }

    private void sendProgress(float percentage) {
        Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION_PROGRESS);
        localIntent.putExtra(Constants.BROADCAST_EXTRAS_PROGRESS, percentage);
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
    }

    public void setAppIsInBackground(boolean appIsInBackground) {
        this.appIsInBackground = appIsInBackground;
        Log.d(TAG, "App background status changed " + this.appIsInBackground);
    }
}
