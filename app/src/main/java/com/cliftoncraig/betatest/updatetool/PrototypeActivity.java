package com.cliftoncraig.betatest.updatetool;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.cliftoncraig.ui.PhotoCycler;
import com.cliftoncraig.ui.SunriseView;

/**
 * Created by clifton
 * Copyright 6/10/15.
 */
public class PrototypeActivity extends Activity {

    private SunriseView sunriseView;
    Handler handler = new Handler();
    PhotoCycler photoCycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prototype);
        sunriseView = (SunriseView) findViewById(R.id.circleView);
        sunriseView.hide();
        photoCycler = new PhotoCycler(this, (ImageView)findViewById(R.id.firstImageView), (ImageView)findViewById(R.id.secondImageView));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            handler.postDelayed(createStartingAnimations(),1000);
        }
    }

    private Runnable createStartingAnimations() {
        return new Runnable() {
            @Override
            public void run() {
                sunriseView.rise();
                photoCycler.cycleImages();
            }
        };
    }

    public void onGoClicked(View sender) {
    }
}
