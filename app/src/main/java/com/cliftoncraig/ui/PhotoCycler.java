package com.cliftoncraig.ui;

import android.content.Context;
import android.os.Handler;
import android.widget.ImageView;

import com.cliftoncraig.betatest.updatetool.R;

/**
 * Created by clifton
 * Copyright 6/10/15.
 */
public class PhotoCycler {
    private final int[] photos = new int[]{R.drawable.photo1, R.drawable.photo2, R.drawable.photo3, R.drawable.photo4, R.drawable.photo5};
    private final Context context;
    int nextPhoto = 1;
    private boolean firstShowing = true;
    private ImageView second;
    public static final int SECONDS = 1000;
    public static final int CYCLE_DELAY = 10 * SECONDS;
    public static final long FADE_DELAY = SECONDS;
    private ImageView first;
    Handler handler;
    private Runnable imageCycleRunnable;

    public PhotoCycler(Context context, ImageView first, ImageView second) {
        this.context = context;
        this.first = first;
        this.second = second;
        this.handler = new Handler();
    }

    private void showImage(int photoResId) {
        if(firstShowing){
            second.setImageDrawable(context.getResources().getDrawable(photoResId));
            second.animate().alpha(1.0f).setDuration(FADE_DELAY);
            first.animate().alpha(0.0f).setDuration(FADE_DELAY);
        }else {
            first.setImageDrawable(context.getResources().getDrawable(photoResId));
            second.animate().alpha(0.0f).setDuration(FADE_DELAY);
            first.animate().alpha(1.0f).setDuration(FADE_DELAY);
        }

        firstShowing = !firstShowing;
    }

    public void cycleImages() {
        stop();
        imageCycleRunnable = new Runnable() {
            public void run() {
                showImage(photos[nextPhoto++]);
                nextPhoto = nextPhoto >= photos.length ? 0 : nextPhoto;
                cycleImages();
            }
        };
        handler.postDelayed(imageCycleRunnable, CYCLE_DELAY);
    }

    public void stop() {
        handler.removeCallbacks(imageCycleRunnable);
    }
}
