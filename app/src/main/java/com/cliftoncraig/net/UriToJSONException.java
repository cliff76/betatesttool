package com.cliftoncraig.net;

import java.net.MalformedURLException;

/**
 * Created by clifton
 * Copyright 6/9/15.
 */
public class UriToJSONException extends Exception {
    public UriToJSONException(String message, Throwable cause) {
        super(message, cause);
    }
}
