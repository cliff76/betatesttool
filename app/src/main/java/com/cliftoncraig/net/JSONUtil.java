package com.cliftoncraig.net;

import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by clifton
 * Copyright 6/8/15.
 */
public class JSONUtil {
    private final String TAG = getClass().getName();

    public JSONObject getJsonObject(Uri uri) throws UriToJSONException {
        String jsonResponse = getJSONString(uri);
        try { return new JSONObject(jsonResponse); }
        catch (JSONException e) { throw  newUriToJSONException("Could not parse JSON response [" + jsonResponse + "]", e); }
    }

    private String getJSONString(Uri uri) throws UriToJSONException {
        final URL url;
        try { url = new URL(uri.toString()); }
        catch (MalformedURLException e) { throw newUriToJSONException("Invalid URI " + uri, e); }
        final InputStream inputStream;
        try { inputStream = url.openStream(); }
        catch (IOException e) { throw newUriToJSONException("Could not connect to URI " + uri, e); }
        return asString(inputStream);
    }

    private UriToJSONException newUriToJSONException(String message, Throwable cause) {
        Log.e(TAG, message, cause);
        cause.printStackTrace();
        return new UriToJSONException(message, cause);
    }

    private String asString(InputStream inputStream) {
        StringBuilder response = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            for(String line = reader.readLine(); line != null; line = reader.readLine()) {
                response.append(line);
            }
        } catch (IOException e) {
            Log.e(TAG,"Error reading response from server.", e);
            e.printStackTrace();
        }
        return response.toString();
    }

    /**
     * Converts a JSON Object to a map, removing the hassle of dealing with checked exceptions in
     * the JSONObject method signatures.
     * @param jsonObject contains key/value mappings
     * @param keys the keys which will be puled out of the JSONObject.
     * @return a map containing the specified keys from the given JSONObject.
     */
    public Map<String, String> toMapWithKeys(JSONObject jsonObject, String... keys) {
        Map<String, String> result = new HashMap<>();
        for(String eachKey : keys) {
            try { result.put(eachKey, jsonObject.getString(eachKey)); }
            catch (JSONException e) {
                Log.e(TAG, "Exception while mapping key " + eachKey,e);
                e.printStackTrace();
            }
        }
        return result;
    }
}
