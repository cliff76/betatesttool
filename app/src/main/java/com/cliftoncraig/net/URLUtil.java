package com.cliftoncraig.net;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by clifton
 * Copyright 6/10/15.
 */
public class URLUtil {

    public HttpURLConnection getHeadHttpURLConnection(String url) {
        try { return getHeadHttpURLConnection(new URL(url)); }
        catch (MalformedURLException e) { throw new RuntimeException("Invalid URL string " + url, e); }
    }

    public HttpURLConnection getHeadHttpURLConnection(URL url) {
        final HttpURLConnection connection = getHttpURLConnection(url);
        try { connection.setRequestMethod("HEAD"); }
        catch (ProtocolException e) {
            final String message = "Can't set request method to HEAD. (Did you open the connection before setting the request method?)";
            throw new RuntimeException(message);
        }
        return connection;
    }

    public HttpURLConnection getHttpURLConnection(URL url) {
        final HttpURLConnection urlConnection;
        try { urlConnection = (HttpURLConnection) url.openConnection(); }
        catch (IOException e) { throw new RuntimeException("Could not open HTTP connection to " + url, e); }
        return urlConnection;
    }

}
